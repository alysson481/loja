module Api 
    module V1
        class ProductsController < ApplicationController
            protect_from_forgery with: :null_session

            def index 
                if(params.has_key?(:category))
                    products = Product.where(category: params[:category])
                    render json: {status: 'SUSSES', message:'filtered products', data: products},status: :ok
                elsif
                    (params.has_key?(:name))
                        products = Product.where(name: params[:name])
                        render json: {status: 'SUSSES', message:'filtered products', data: products},status: :ok
                elsif
                    (params.has_key?(:price))
                        products = Product.where(price: params[:price])
                        render json: {status: 'SUSSES', message:'filtered products', data: products},status: :ok
                 elsif
                    (params.has_key?(:quantity))
                        products = Product.where(quantity: params[:quantity])
                        render json: {status: 'SUSSES', message:'filtered products', data: products},status: :ok        
                elsif
                    products = Product.search(params[:search]) 
                    render json: {status: 'SUSSES', message:'search results', data: products},status: :ok
                else    
                products = Product.order('created_at DESC');
                render json: {status: 'SUSSES', message:'Loaded products', data: products},status: :ok
                end
            
            end
            def show  
                products = Product.find(params[:id])
                render json: {status: 'SUSSES', message:'Loaded products', data: products},status: :ok
            end
            def create
                products = Product.new(product_params)
                
                if products.save
                    render json: {status: 'SUSSES', message:'Saved product', data: products},status: :ok
                else
                    render json: {status: 'ERROR', message:'Product not saved', data: products.error},status: :unprocessable_entity
                end
            end
            def destroy
                products = Product.find(params[:id])
                products.destroy
                render json: {status: 'SUSSES', message:'Deleted products', data: products},status: :ok
            end
            def update
                products = Product.find(params[:id])
                if products.update_attributes(product_params)
                render json: {status: 'SUSSES', message:'Updated products', data: products},status: :ok
                else
                render json: {status: 'ERROR', message:'Product not updated', data: products.error},status: :unprocessable_entity
                end
            end
            private

            def product_params
                    params.permit(:name, :category, :quantity, :price, :descritpion)
            end
        end         
    end
end
